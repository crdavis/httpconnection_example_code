package com.mycompany.httpconnection_example_code;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;

/**
 *
 * @author Carlton Davis
 */

//Test class to test the HTTPURLConnectionEX
public class HTTPURLConnectionEXTest {
    
    //String containing the website to used for the testing
    public String website;
    
    //String containing the query to be made
    public String str;
    
    //This class field stores the HTTPURLConnectionEX object to be used for the tests
    public HTTPURLConnectionEX theObject;
   
    
    //This method will be executed before all Test methods.
    @BeforeEach
    public void setUP() {
        //Create the object that will be used for the tests
        theObject = new HTTPURLConnectionEX(); 
        
         //Initializes the String objects
        website = "https://toolbox.googleapps.com/apps/dig/#A/";
        str = "cbc.ca";   
    }
           

    /**
     * Test of sendQuery method, of class HTTPURLConnectionEX.
     */
    @Test
    public void testSendQuery() throws Exception {
        System.out.println("sendQuery");
        
        //Send the query to the web server
        String response = theObject.sendQuery(website, str);
        
        //Assert that the response is not null
        assertNotNull(response);
        
        //Determine whether the response from the serve contains the substring "Error"
        assertTrue(response.toLowerCase().contains("dns lookup"));
    }
    
}
