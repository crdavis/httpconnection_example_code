package com.mycompany.httpconnection_example_code;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 * @author Carlton Davis
 */

/* Class to send queries to a web server
   and receive response.
*/
public class HTTPURLConnectionEX {
    
    //Method to send query to the server
    String sendQuery(String website, String parameters) throws IOException {
       
       //Stores data the web server returns 
       StringBuilder data  = new StringBuilder();
       
        try {
           //Create the URL
           URL url = new URL(website + parameters);
           
           //Open the connection
           HttpURLConnection conn = (HttpURLConnection) url.openConnection();
           
           //Get the response code
           int responseCode = conn.getResponseCode();
           if (responseCode == HttpURLConnection.HTTP_OK) {
               
               //Setup input stream
               InputStream inStream = conn.getInputStream();
               
               try ( //Wrap the InputSteam in a BufferReader for reading data to Strings
                     BufferedReader in = new BufferedReader(new InputStreamReader(inStream))) {
                   String line;
                   //Read the server response one line at a time
                   while ((line = in.readLine()) != null) {
                       data.append("\n"+line);
                   }
                   //Close the BufferedReader
               }
               
           } else {
                System.err.println("Wrong response code: " + responseCode);
            }
           
           
       } catch (IOException e) {
                System.out.println(e.getMessage());
       }
        
        return data.toString();
    }
    
    
    public static void main(String args[]) throws IOException {
        
        //Use Google dig tool
        String website = "https://toolbox.googleapps.com/apps/dig/#A/";
        
        //We will send a query to get information about cbc.ca site
        String str = "?q=cbc.ca";
        String response;
        
        HTTPURLConnectionEX myQuery = new HTTPURLConnectionEX();
        
        //Send the query and receive the response
        response = myQuery.sendQuery(website, str);
        
        System.out.print("Response from the server: \n" + response);
        
    }   
    
}
